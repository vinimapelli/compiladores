from tokenizer import tokenize

def busca(dir,token,regra):
    var = ""
    try:
        return dir[regra][token]
    except:
        return  print("ERRRO")

dicionario = {
    'E':{'id':'TS','num':'TS','(':'TS'},
    'T':{'id':'FG','num':'FG','(':'FG'},
    'S':{'+':'+TS','-':'-TS',')':'$','$':'$'},
    'G':{'+':'$','-':'$','*':'*FG','/':'/FG',')':'$','$':'$'},
    'F':{'id':'id','num':'num','(':'(E)'}
}

entrada = input()
cadeia=[]

for tokens in tokenize(entrada):
    if(tokens.txt != None):
        cadeia.append(tokens.txt)

cadeia.append('$')
pilha=['$','E']
while(True):

    if(pilha[len(pilha)-1] == cadeia[0]):
        print("PILHA:", pilha, "CADEIA:", cadeia, "REGRA: -------")
        print("RECONHECEU O TOKEN",pilha[len(pilha)-1])
        del(pilha[len(pilha)-1])
        del(cadeia[0])

    string = busca(dicionario,cadeia[0],pilha[len(pilha)-1])
    aux=string
    print("PILHA:", pilha, "CADEIA:", cadeia, "REGRA:", pilha[len(pilha) - 1], "->", aux)
    if(string == "$"):
        del(pilha[len(pilha)-1])
    else: 
        if(string != cadeia[0]):
            string = reversed(string)
            lista = list(string)
        else:
            lista = string
        if(type(lista)!=list):
            if(lista == cadeia[0]):
                pilha[len(pilha)-1] = lista
        else:
            for i in range(len(lista)):
                if(i==0):
                    pilha[len(pilha)-1] = lista[0]
                else:
                    pilha.append(lista[i])
    if(pilha[len(pilha)-1]=="$" and cadeia[0]=="$"):
        print("PILHA:", pilha, "CADEIA:", cadeia, "REGRA:", pilha[len(pilha) - 1], "->", aux)
        break

print("Palavra ",entrada," foi reconhecida")